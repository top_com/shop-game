package com.ruoyi.web.controller.system;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NotFoundController {

    @GetMapping("/404")
    public String index(ModelMap mmap){
        return "front/404";
    }
}
