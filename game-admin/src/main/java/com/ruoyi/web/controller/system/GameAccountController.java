package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.system.domain.GameSpu;
import com.ruoyi.system.domain.GameType;
import com.ruoyi.system.service.IGameSpuService;
import com.ruoyi.system.service.IGameTypeService;
import com.ruoyi.util.AccountUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.GameAccount;
import com.ruoyi.system.service.IGameAccountService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品 - 出售账号的Controller
 * 
 * @author hxp
 * @date 2021-12-07
 */
@Controller
@RequestMapping("/system/account")
public class GameAccountController extends BaseController
{
    private String prefix = "system/account";

    @Autowired
    private IGameAccountService gameAccountService;
    @Autowired
    private IGameTypeService gameTypeService;
    @Autowired
    private IGameSpuService gameSpuService;

    @RequiresPermissions("system:account:view")
    @GetMapping()
    public String account()
    {
        return prefix + "/account";
    }

    /**
     * 查询商品 - 出售账号的列表
     */
    @RequiresPermissions("system:account:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GameAccount gameAccount)
    {
        if(!AccountUtil.mainAccount(ShiroUtils.getLoginName())){
            gameAccount.setCreateBy(ShiroUtils.getLoginName());
        }
        startPage();
        List<GameAccount> list = gameAccountService.selectGameAccountList(gameAccount);
        for (GameAccount account : list) {
            GameType gameType = gameTypeService.selectGameTypeById(account.getGameTypeId());
            String gameName = gameType == null? "游戏已删除": gameType.getGname();
            account.setGameName(gameName);
        }
        return getDataTable(list);
    }

    /**
     * 导出商品 - 出售账号的列表
     */
    @RequiresPermissions("system:account:export")
    @Log(title = "账号列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GameAccount gameAccount)
    {
        if(!AccountUtil.mainAccount(ShiroUtils.getLoginName())){
            gameAccount.setCreateBy(ShiroUtils.getLoginName());
        }
        List<GameAccount> list = gameAccountService.selectGameAccountList(gameAccount);
        ExcelUtil<GameAccount> util = new ExcelUtil<GameAccount>(GameAccount.class);
        return util.exportExcel(list, "商品 - 出售账号的数据");
    }

    /**
     * 新增商品 - 出售账号的
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        List<GameType> gameTypes = gameTypeService.selectGameTypeList(new GameType());
        map.put("gameList",gameTypes);
        return prefix + "/add";
    }

    /**
     * 新增保存商品 - 出售账号的
     */
    @RequiresPermissions("system:account:add")
    @Log(title = "商品账号添加", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GameAccount gameAccount)
    {
        gameAccount.setDetail(this.getRequest().getParameter("detail"));
        return toAjax(gameAccountService.insertGameAccount(gameAccount));
    }

    /**
     * 修改商品 - 出售账号的
     */
    @RequiresPermissions("system:account:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        GameAccount gameAccount = gameAccountService.selectGameAccountById(id);
        mmap.put("gameAccount", gameAccount);
        List<GameType> gameTypes = gameTypeService.selectGameTypeList(new GameType());
        mmap.put("gameList",gameTypes);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品 - 出售账号的
     */
    @RequiresPermissions("system:account:edit")
    @Log(title = "商品 - 出售账号的", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GameAccount gameAccount)
    {
        gameAccount.setDetail(this.getRequest().getParameter("detail"));
        return toAjax(gameAccountService.updateGameAccount(gameAccount));
    }

    /**
     * 删除商品 - 出售账号的
     */
    @RequiresPermissions("system:account:remove")
    @Log(title = "商品 - 出售账号的", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(gameAccountService.deleteGameAccountByIds(ids));
    }

    /**
     * 新增商品 - 出售账号的
     */
    @GetMapping("/setCodition/{typeid}")
    public String setCodition(@PathVariable Integer typeid, ModelMap map)
    {
        GameSpu  gameSpu = new GameSpu();
        gameSpu.setGameTypeId(Long.valueOf(typeid));
        List<GameSpu> gsList = gameSpuService.selectGameSpuList(gameSpu);
        StringBuilder builder = new StringBuilder();
        int radio_index = 0;
        int checkbox_index = 0;
        for (GameSpu  gs:gsList) {
            /**
             * <label class="checkbox-inline check-box"><input type="checkbox" value="option1">a</label>
             * <label class="radio-inline radio-box"><input type="radio" name="sex" value="option3">c</label>
             */
            String type = gs.getFormType();
            builder.append("<div class='row'>");
            builder.append("<div class='form-group'>");
            builder.append("<label class='col-sm-3 control-label'>"+gs.getGameSpuName()+"：</label>");
            builder.append("<div class='col-sm-8'>");
            String[] values = gs.getGameValue().split(",");
            if("0".equals(type)){  //单选
                String  name = "radio" + radio_index;
                for (String  value :values) {
                    builder.append("<label class='radio-inline radio-box'><input type='radio' data='"+value+"' name='"+name+"' value='"+value+"'>"+value+"</label>");
                }
                radio_index ++;
            }
            if("1".equals(type)){ //多选
                String  name = "checkbox" + checkbox_index;
                for (String  value :values) {
                    builder.append("<label class='checkbox-inline check-box'><input type='checkbox' data='"+value+"' name='"+name+"' value='"+value+"'>"+value+"</label>");
                }
                checkbox_index++;
            }
            builder.append("</div>");
            builder.append("</div>");
            builder.append("</div>");
        }
        map.put("htmlText",builder.toString());
        return prefix + "/setcodition";
    }

}
