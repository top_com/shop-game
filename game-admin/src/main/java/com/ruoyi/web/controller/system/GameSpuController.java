package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.system.domain.GameType;
import com.ruoyi.system.service.IGameTypeService;
import com.ruoyi.util.AccountUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.GameSpu;
import com.ruoyi.system.service.IGameSpuService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游戏spuController
 * 
 * @author hxp
 * @date 2021-12-06
 */
@Controller
@RequestMapping("/system/spu")
public class GameSpuController extends BaseController
{
    private String prefix = "system/spu";

    @Autowired
    private IGameTypeService gameTypeService;

    @Autowired
    private IGameSpuService gameSpuService;

    @RequiresPermissions("system:spu:view")
    @GetMapping()
    public String spu(ModelMap modelMap)
    {
        List<GameType> gamelist = gameTypeService.selectGameTypeList(new GameType());
        modelMap.put("gamelist",gamelist);
        return prefix + "/spu";
    }

    /**
     * 查询游戏spu列表
     */
    @RequiresPermissions("system:spu:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GameSpu gameSpu)
    {
        if(!AccountUtil.mainAccount(ShiroUtils.getLoginName())){
            gameSpu.setCreateBy(ShiroUtils.getLoginName());
        }
        startPage();
        List<GameSpu> list = gameSpuService.selectGameSpuList(gameSpu);
        for (GameSpu  spu:list) {
            spu.setGameName(gameTypeService.selectGameTypeById(spu.getGameTypeId()).getGname());
        }
        return getDataTable(list);
    }

    /**
     * 导出游戏spu列表
     */
    @RequiresPermissions("system:spu:export")
    @Log(title = "游戏spu", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GameSpu gameSpu)
    {
        if(!AccountUtil.mainAccount(ShiroUtils.getLoginName())){
            gameSpu.setCreateBy(ShiroUtils.getLoginName());
        }
        List<GameSpu> list = gameSpuService.selectGameSpuList(gameSpu);
        ExcelUtil<GameSpu> util = new ExcelUtil<GameSpu>(GameSpu.class);
        return util.exportExcel(list, "游戏spu数据");
    }

    /**
     * 新增游戏spu
     */
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        List<GameType> gamelist = gameTypeService.selectGameTypeList(new GameType());
        modelMap.put("gamelist",gamelist);
        return prefix + "/add";
    }

    /**
     * 新增保存游戏spu
     */
    @RequiresPermissions("system:spu:add")
    @Log(title = "游戏spu", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GameSpu gameSpu)
    {
        return toAjax(gameSpuService.insertGameSpu(gameSpu));
    }

    /**
     * 修改游戏spu
     */
    @RequiresPermissions("system:spu:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        GameSpu gameSpu = gameSpuService.selectGameSpuById(id);
        gameSpu.setGameName(gameTypeService.selectGameTypeById(gameSpu.getGameTypeId()).getGname());
        mmap.put("gameSpu", gameSpu);
        List<GameType> gamelist = gameTypeService.selectGameTypeList(new GameType());
        mmap.put("gamelist",gamelist);
        return prefix + "/edit";
    }

    /**
     * 修改保存游戏spu
     */
    @RequiresPermissions("system:spu:edit")
    @Log(title = "游戏spu", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GameSpu gameSpu)
    {
        return toAjax(gameSpuService.updateGameSpu(gameSpu));
    }

    /**
     * 删除游戏spu
     */
    @RequiresPermissions("system:spu:remove")
    @Log(title = "游戏spu", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(gameSpuService.deleteGameSpuByIds(ids));
    }
}
