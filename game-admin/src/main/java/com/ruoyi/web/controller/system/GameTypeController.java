package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.util.AccountUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.GameType;
import com.ruoyi.system.service.IGameTypeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游戏大类Controller
 * 
 * @author hxp
 * @date 2021-12-03
 */
@Controller
@RequestMapping("/system/type")
public class GameTypeController extends BaseController
{
    private String prefix = "system/type";

    @Autowired
    private IGameTypeService gameTypeService;

    @RequiresPermissions("system:type:view")
    @GetMapping()
    public String type()
    {
        return prefix + "/type";
    }

    /**
     * 查询游戏大类列表
     */
    @RequiresPermissions("system:type:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GameType gameType)
    {
        if(!AccountUtil.mainAccount(ShiroUtils.getLoginName())){
            gameType.setCreateBy(ShiroUtils.getLoginName());
        }
        startPage();
        List<GameType> list = gameTypeService.selectGameTypeList(gameType);
        return getDataTable(list);
    }

    /**
     * 导出游戏大类列表
     */
    @RequiresPermissions("system:type:export")
    @Log(title = "游戏大类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GameType gameType)
    {
        if(!AccountUtil.mainAccount(ShiroUtils.getLoginName())){
            gameType.setCreateBy(ShiroUtils.getLoginName());
        }
        List<GameType> list = gameTypeService.selectGameTypeList(gameType);
        ExcelUtil<GameType> util = new ExcelUtil<GameType>(GameType.class);
        return util.exportExcel(list, "游戏大类数据");
    }

    /**
     * 新增游戏大类
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存游戏大类
     */
    @RequiresPermissions("system:type:add")
    @Log(title = "游戏大类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GameType gameType)
    {
        return toAjax(gameTypeService.insertGameType(gameType));
    }

    /**
     * 修改游戏大类
     */
    @RequiresPermissions("system:type:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        GameType gameType = gameTypeService.selectGameTypeById(id);
        mmap.put("gameType", gameType);
        return prefix + "/edit";
    }

    /**
     * 修改保存游戏大类
     */
    @RequiresPermissions("system:type:edit")
    @Log(title = "游戏大类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GameType gameType)
    {
        return toAjax(gameTypeService.updateGameType(gameType));
    }

    /**
     * 删除游戏大类
     */
    @RequiresPermissions("system:type:remove")
    @Log(title = "游戏大类", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(gameTypeService.deleteGameTypeByIds(ids));
    }
}
