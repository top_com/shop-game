package com.ruoyi.web.controller.system;

import java.util.Date;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.GameAccount;
import com.ruoyi.system.domain.GameSpu;
import com.ruoyi.system.domain.GameType;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.ShiroConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.CookieUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.shiro.service.SysPasswordService;

/**
 * 首页 业务处理
 * 
 * @author ruoyi
 */
@Controller
public class SysIndexController extends BaseController
{
    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private SysPasswordService passwordService;

    @Autowired
    private IGameTypeService gameTypeService;
    @Autowired
    private IGameAccountService gameAccountService;
    @Autowired
    private IGameSpuService gameSpuService;

    @GetMapping("/")
    public String  frontIndex(ModelMap mmap){
        List<GameType> gameTypes = gameTypeService.selectGameTypeList(new GameType());
        mmap.put("gameList",gameTypes);
        return "front/index";
    }

    /**
     * 前端页面请求数据
     * @param typeId
     * @return
     */
    @PostMapping("/listData")
    @ResponseBody
    public AjaxResult listData(String typeId){
        GameAccount gameAccount = new GameAccount();
        gameAccount.setGameTypeId(Long.valueOf(typeId));
        gameAccount.setShowStatus("0");
        List<GameAccount> gameAccounts = gameAccountService.selectGameAccountList(gameAccount);
        for (GameAccount account : gameAccounts) {
            account.setGameName(gameTypeService.selectGameTypeById(account.getGameTypeId()).getGname());
        }
        return AjaxResult.success(gameAccounts);
    }


    @GetMapping("/list")
    public String  type(Integer id,ModelMap mmap){
        GameType gameType = gameTypeService.selectGameTypeById(Long.valueOf(id));
        mmap.put("gameType",gameType);
        //生成动态条件
        GameSpu gameSpu = new GameSpu();
        gameSpu.setGameTypeId(Long.valueOf(id));
        List<GameSpu> gsList = gameSpuService.selectGameSpuList(gameSpu);
        StringBuilder builder = new StringBuilder();
        int radio_index = 0;
        int checkbox_index = 0;
        for (GameSpu  gs:gsList) {
            /**
             * <label><input type="radio" name="v1">条件1</label>
             * <label><input type='checkbox' name='v2'>条件2</label>
             */
            String type = gs.getFormType();
            builder.append("<div class='item1'>");
            builder.append("<b>"+gs.getGameSpuName()+"</b>");
            String[] values = gs.getGameValue().split(",");
            if("0".equals(type)){  //单选
                String  name = "radio" + radio_index;
                for (String  value :values) {
                    builder.append("<label><input type='radio' name='"+name+"' value='"+value+"'>"+value+"</label>");
                }
                radio_index ++;
            }
            if("1".equals(type)){ //多选
                String  name = "checkbox" + checkbox_index;
                for (String  value :values) {
                    builder.append("<label><input type='checkbox' name='"+name+"' value='"+value+"'>"+value+"</label>");
                }
                checkbox_index++;
            }
            builder.append("</div>");
        }
        mmap.put("htmlText",builder.toString());
        return "front/list";
    }

    @GetMapping("/select")
    public String  select(ModelMap mmap){
        return "front/select";
    }

    @GetMapping("/detail/{id}.html")
    public String  info(@PathVariable Integer id,ModelMap mmap){
        GameAccount gameAccount = gameAccountService.selectGameAccountById(Long.valueOf(id));
        gameAccount.setGameName(gameTypeService.selectGameTypeById(gameAccount.getGameTypeId()).getGname());
        mmap.put("gameAccount",gameAccount);
        return "front/detail";
    }


    @GetMapping("/answer")
    public String  answer(ModelMap mmap){
        return "front/answer";
    }

    @GetMapping("/question")
    public String  question(ModelMap mmap){
        return "front/question";
    }


    @GetMapping("/lc")
    public String  lc(ModelMap mmap){
        return "front/lc";
    }

    @Value("${kehu.weixin}")
    private String kehuUrl;
    @Value("${kehu.haoma}")
    private String haoma;
    @GetMapping("/wx")
    public String  wx(ModelMap mmap){
        mmap.put("weixin",kehuUrl);
        mmap.put("haoma",haoma);
        return "front/wx";
    }




    // 系统后台首页
    @GetMapping("/index")
    public String index(ModelMap mmap)
    {
        // 取身份信息
        SysUser user = getSysUser();
        // 根据用户id取出菜单
        List<SysMenu> menus = menuService.selectMenusByUser(user);
        mmap.put("menus", menus);
        mmap.put("user", user);
        mmap.put("sideTheme", configService.selectConfigByKey("sys.index.sideTheme"));
        mmap.put("skinName", configService.selectConfigByKey("sys.index.skinName"));
        Boolean footer = Convert.toBool(configService.selectConfigByKey("sys.index.footer"), true);
        Boolean tagsView = Convert.toBool(configService.selectConfigByKey("sys.index.tagsView"), true);
        mmap.put("footer", footer);
        mmap.put("tagsView", tagsView);
        mmap.put("mainClass", contentMainClass(footer, tagsView));
        mmap.put("copyrightYear", RuoYiConfig.getCopyrightYear());
        mmap.put("demoEnabled", RuoYiConfig.isDemoEnabled());
        mmap.put("isDefaultModifyPwd", initPasswordIsModify(user.getPwdUpdateDate()));
        mmap.put("isPasswordExpired", passwordIsExpiration(user.getPwdUpdateDate()));
        mmap.put("isMobile", ServletUtils.checkAgentIsMobile(ServletUtils.getRequest().getHeader("User-Agent")));

        // 菜单导航显示风格
        String menuStyle = configService.selectConfigByKey("sys.index.menuStyle");
        // 移动端，默认使左侧导航菜单，否则取默认配置
        String indexStyle = ServletUtils.checkAgentIsMobile(ServletUtils.getRequest().getHeader("User-Agent")) ? "index" : menuStyle;

        // 优先Cookie配置导航菜单
        Cookie[] cookies = ServletUtils.getRequest().getCookies();
        for (Cookie cookie : cookies)
        {
            if (StringUtils.isNotEmpty(cookie.getName()) && "nav-style".equalsIgnoreCase(cookie.getName()))
            {
                indexStyle = cookie.getValue();
                break;
            }
        }
        String webIndex = "topnav".equalsIgnoreCase(indexStyle) ? "index-topnav" : "index";
        return webIndex;
    }

    // 锁定屏幕
    @GetMapping("/lockscreen")
    public String lockscreen(ModelMap mmap)
    {
        mmap.put("user", getSysUser());
        ServletUtils.getSession().setAttribute(ShiroConstants.LOCK_SCREEN, true);
        return "lock";
    }

    // 解锁屏幕
    @PostMapping("/unlockscreen")
    @ResponseBody
    public AjaxResult unlockscreen(String password)
    {
        SysUser user = getSysUser();
        if (StringUtils.isNull(user))
        {
            return AjaxResult.error("服务器超时，请重新登录");
        }
        if (passwordService.matches(user, password))
        {
            ServletUtils.getSession().removeAttribute(ShiroConstants.LOCK_SCREEN);
            return AjaxResult.success();
        }
        return AjaxResult.error("密码不正确，请重新输入。");
    }

    // 切换主题
    @GetMapping("/system/switchSkin")
    public String switchSkin()
    {
        return "skin";
    }

    // 切换菜单
    @GetMapping("/system/menuStyle/{style}")
    public void menuStyle(@PathVariable String style, HttpServletResponse response)
    {
        CookieUtils.setCookie(response, "nav-style", style);
    }

    // 系统介绍
    @GetMapping("/system/main")
    public String main(ModelMap mmap)
    {
        mmap.put("version", RuoYiConfig.getVersion());
        return "main";
    }

    // content-main class
    public String contentMainClass(Boolean footer, Boolean tagsView)
    {
        if (!footer && !tagsView)
        {
            return "tagsview-footer-hide";
        }
        else if (!footer)
        {
            return "footer-hide";
        }
        else if (!tagsView)
        {
            return "tagsview-hide";
        }
        return StringUtils.EMPTY;
    }

    // 检查初始密码是否提醒修改
    public boolean initPasswordIsModify(Date pwdUpdateDate)
    {
        Integer initPasswordModify = Convert.toInt(configService.selectConfigByKey("sys.account.initPasswordModify"));
        return initPasswordModify != null && initPasswordModify == 1 && pwdUpdateDate == null;
    }

    // 检查密码是否过期
    public boolean passwordIsExpiration(Date pwdUpdateDate)
    {
        Integer passwordValidateDays = Convert.toInt(configService.selectConfigByKey("sys.account.passwordValidateDays"));
        if (passwordValidateDays != null && passwordValidateDays > 0)
        {
            if (StringUtils.isNull(pwdUpdateDate))
            {
                // 如果从未修改过初始密码，直接提醒过期
                return true;
            }
            Date nowDate = DateUtils.getNowDate();
            return DateUtils.differentDaysByMillisecond(nowDate, pwdUpdateDate) > passwordValidateDays;
        }
        return false;
    }
}
