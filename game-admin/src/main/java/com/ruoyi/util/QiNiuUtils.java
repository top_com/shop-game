package com.ruoyi.util;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.InputStream;

/**
 * 七牛云上传
 */
@org.springframework.context.annotation.Configuration
public class QiNiuUtils {

    @Autowired
    QiniuConfig qiniuConfig;

    /**
     * 获取上传的upToken
     * @return
     */
    private  String getUpToken(){
        Auth auth = Auth.create(qiniuConfig.getAccessKey(), qiniuConfig.getSecretKey());
        String upToken = auth.uploadToken(qiniuConfig.getBucket());
        return upToken;
    }

    /**
     * 获取上传的配置
     * @return
     */
    private static UploadManager getUploadManager(){
        Configuration cfg = new Configuration(Region.huanan());
        cfg.useHttpsDomains = false;
        return new UploadManager(cfg);
    }

    /**
     * 文件上传
     * @param filepath
     * @param key
     * @return
     */
    public String uploadFile(String filepath,String key){
        try {
            String upToken = this.getUpToken();
            UploadManager uploadManager = this.getUploadManager();
            Response response = uploadManager.put(filepath, key, upToken);
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            System.out.println(putRet.key);
            System.out.println(putRet.hash);
            return putRet.key;
        } catch (QiniuException ex) {
            ex.printStackTrace();
        }
        return  null;
    }

    /**
     * 上传文本字节数组
     * @param bytes
     * @param key
     * @return
     */
    public String uploadByteArray(byte[] bytes,String key){
        try {
            String upToken = getUpToken();
            UploadManager uploadManager = this.getUploadManager();
            Response response = uploadManager.put(bytes, key, upToken);
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            System.out.println(putRet.key);
            System.out.println(putRet.hash);
            return putRet.key;
        } catch (QiniuException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 上传数据流
     * @param in
     * @param key
     * @return
     */
    public String uploadInputStream(InputStream in, String key){
        try {
            String upToken = getUpToken();
            UploadManager uploadManager = this.getUploadManager();
            Response response = uploadManager.put(in, key, upToken,null,null);
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            System.out.println(putRet.key);
            System.out.println(putRet.hash);
            return putRet.key;
        } catch (QiniuException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 删除空间中的文件
     * @param key
     * @return
     */
    public boolean deleteFile(String key){
        try{
            Auth auth = Auth.create(qiniuConfig.getAccessKey(), qiniuConfig.getSecretKey());
            Configuration cfg = new Configuration(Region.huanan());
            cfg.useHttpsDomains = false;
            BucketManager bucketManager = new BucketManager(auth, cfg);
            bucketManager.delete(qiniuConfig.getBucket(), key);
            return true;
        }catch (QiniuException e){
            e.printStackTrace();
        }
        return  false;
    }
}
