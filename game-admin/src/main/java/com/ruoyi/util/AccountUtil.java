package com.ruoyi.util;

import java.util.ArrayList;
import java.util.List;

public class AccountUtil {

    private static List<String>  mainAccounts = new ArrayList<>();

    static {
        mainAccounts.add("szwy");
        mainAccounts.add("admin");
        mainAccounts.add("姚总");
    }

    public static boolean mainAccount(String loginName){
        return mainAccounts.contains(loginName);
    }
}
