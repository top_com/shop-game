package com.ruoyi.system.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 游戏spu对象 tb_game_spu
 * @author hxp
 * @date 2021-12-06
 */
public class GameSpu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 游戏账号分类ID */
    @Excel(name = "游戏账号分类ID")
    private Long gameTypeId;


    /** 游戏账号条件的spu_name */
    @Excel(name = "游戏账号条件的spu_name")
    private String gameSpuName;

    @Excel(name = "游戏条件的value")
    private String gameValue;
    @Excel(name = "渲染表单类型 多个值用,分割")
    private String formType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGameTypeId(Long gameTypeId) 
    {
        this.gameTypeId = gameTypeId;
    }

    public Long getGameTypeId() 
    {
        return gameTypeId;
    }
    public void setGameSpuName(String gameSpuName) 
    {
        this.gameSpuName = gameSpuName;
    }

    public String getGameSpuName() 
    {
        return gameSpuName;
    }


    public String getGameValue() {
        return gameValue;
    }

    public void setGameValue(String gameValue) {
        this.gameValue = gameValue;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gameTypeId", getGameTypeId())
            .append("gameSpuName", getGameSpuName())
            .append("gameValue", getGameValue())
            .append("formType", getFormType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }

    private String gameName;

    public String getGameName() {
        return gameName;
    }
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
}
