package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品 - 出售账号的对象 tb_game_account
 * 
 * @author hxp
 * @date 2021-12-07
 */
public class GameAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 游戏账号分类ID */
    @Excel(name = "游戏账号分类ID")
    private Long gameTypeId;

    private String  showStatus;

    /** 账号编号 */
    @Excel(name = "账号编号")
    private String accountNo;
    /** 账号编号 */
    @Excel(name = "游戏账号")
    private String gameNo;
    /** 账号标题 */
    @Excel(name = "账号标题")
    private String title;

    /** 账号价格 */
    @Excel(name = "账号价格")
    private BigDecimal price;

    /** 账号说明详情 */
    @Excel(name = "账号说明详情")
    private String detail;

    /** 账号截图 */
    @Excel(name = "账号截图")
    private String imgs;

    /** 账号排序 */
    @Excel(name = "账号排序")
    private int sortNum;

    /** 账号搜索条件 */
    @Excel(name = "账号搜索条件")
    private String searchParam;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGameTypeId(Long gameTypeId) 
    {
        this.gameTypeId = gameTypeId;
    }

    public Long getGameTypeId() 
    {
        return gameTypeId;
    }
    public void setAccountNo(String accountNo) 
    {
        this.accountNo = accountNo;
    }

    public String getGameNo() {
        return gameNo;
    }

    public void setGameNo(String gameNo) {
        this.gameNo = gameNo;
    }

    public String getAccountNo()
    {
        return accountNo;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setDetail(String detail) 
    {
        this.detail = detail;
    }

    public String getDetail() 
    {
        return detail;
    }
    public void setImgs(String imgs) 
    {
        this.imgs = imgs;
    }

    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }

    public String getImgs()
    {
        return imgs;
    }
    public void setSearchParam(String searchParam) 
    {
        this.searchParam = searchParam;
    }

    public String getSearchParam() 
    {
        return searchParam;
    }

    public String getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(String showStatus) {
        this.showStatus = showStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gameTypeId", getGameTypeId())
             .append("showStatus", getShowStatus())
            .append("accountNo", getAccountNo())
            .append("gameNo", getGameNo())
            .append("title", getTitle())
            .append("price", getPrice())
            .append("detail", getDetail())
            .append("imgs", getImgs())
            .append("searchParam", getSearchParam()).append("sortNum", getSortNum())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }

    private String  gameName;

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
}
