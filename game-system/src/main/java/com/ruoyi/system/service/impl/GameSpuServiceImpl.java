package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.GameSpuMapper;
import com.ruoyi.system.domain.GameSpu;
import com.ruoyi.system.service.IGameSpuService;
import com.ruoyi.common.core.text.Convert;

/**
 * 游戏spuService业务层处理
 * 
 * @author hxp
 * @date 2021-12-06
 */
@Service
public class GameSpuServiceImpl implements IGameSpuService 
{
    @Autowired
    private GameSpuMapper gameSpuMapper;

    /**
     * 查询游戏spu
     * 
     * @param id 游戏spu主键
     * @return 游戏spu
     */
    @Override
    public GameSpu selectGameSpuById(Long id)
    {
        return gameSpuMapper.selectGameSpuById(id);
    }

    /**
     * 查询游戏spu列表
     * 
     * @param gameSpu 游戏spu
     * @return 游戏spu
     */
    @Override
    public List<GameSpu> selectGameSpuList(GameSpu gameSpu)
    {
        return gameSpuMapper.selectGameSpuList(gameSpu);
    }

    /**
     * 新增游戏spu
     * 
     * @param gameSpu 游戏spu
     * @return 结果
     */
    @Override
    public int insertGameSpu(GameSpu gameSpu)
    {
        gameSpu.setCreateTime(DateUtils.getNowDate());
        gameSpu.setCreateBy(ShiroUtils.getLoginName());
        return gameSpuMapper.insertGameSpu(gameSpu);
    }

    /**
     * 修改游戏spu
     * 
     * @param gameSpu 游戏spu
     * @return 结果
     */
    @Override
    public int updateGameSpu(GameSpu gameSpu)
    {
        gameSpu.setUpdateTime(DateUtils.getNowDate());
        gameSpu.setUpdateBy(ShiroUtils.getLoginName());
        return gameSpuMapper.updateGameSpu(gameSpu);
    }

    /**
     * 批量删除游戏spu
     * 
     * @param ids 需要删除的游戏spu主键
     * @return 结果
     */
    @Override
    public int deleteGameSpuByIds(String ids)
    {
        return gameSpuMapper.deleteGameSpuByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除游戏spu信息
     * 
     * @param id 游戏spu主键
     * @return 结果
     */
    @Override
    public int deleteGameSpuById(Long id)
    {
        return gameSpuMapper.deleteGameSpuById(id);
    }
}
