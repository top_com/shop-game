package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.GameAccountMapper;
import com.ruoyi.system.domain.GameAccount;
import com.ruoyi.system.service.IGameAccountService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商品 - 出售账号的Service业务层处理
 * 
 * @author hxp
 * @date 2021-12-07
 */
@Service
public class GameAccountServiceImpl implements IGameAccountService 
{
    @Autowired
    private GameAccountMapper gameAccountMapper;

    /**
     * 查询商品 - 出售账号的
     * 
     * @param id 商品 - 出售账号的主键
     * @return 商品 - 出售账号的
     */
    @Override
    public GameAccount selectGameAccountById(Long id)
    {
        return gameAccountMapper.selectGameAccountById(id);
    }

    /**
     * 查询商品 - 出售账号的列表
     * 
     * @param gameAccount 商品 - 出售账号的
     * @return 商品 - 出售账号的
     */
    @Override
    public List<GameAccount> selectGameAccountList(GameAccount gameAccount)
    {
        return gameAccountMapper.selectGameAccountList(gameAccount);
    }

    /**
     * 新增商品 - 出售账号的
     * 
     * @param gameAccount 商品 - 出售账号的
     * @return 结果
     */
    @Override
    public int insertGameAccount(GameAccount gameAccount)
    {
        gameAccount.setCreateTime(DateUtils.getNowDate());
        gameAccount.setCreateBy(ShiroUtils.getLoginName());
        return gameAccountMapper.insertGameAccount(gameAccount);
    }

    /**
     * 修改商品 - 出售账号的
     * 
     * @param gameAccount 商品 - 出售账号的
     * @return 结果
     */
    @Override
    public int updateGameAccount(GameAccount gameAccount)
    {
        gameAccount.setUpdateTime(DateUtils.getNowDate());
        gameAccount.setUpdateBy(ShiroUtils.getLoginName());
        return gameAccountMapper.updateGameAccount(gameAccount);
    }

    /**
     * 批量删除商品 - 出售账号的
     * 
     * @param ids 需要删除的商品 - 出售账号的主键
     * @return 结果
     */
    @Override
    public int deleteGameAccountByIds(String ids)
    {
        return gameAccountMapper.deleteGameAccountByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品 - 出售账号的信息
     * 
     * @param id 商品 - 出售账号的主键
     * @return 结果
     */
    @Override
    public int deleteGameAccountById(Long id)
    {
        return gameAccountMapper.deleteGameAccountById(id);
    }
}
