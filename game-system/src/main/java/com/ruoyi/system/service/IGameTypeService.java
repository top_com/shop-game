package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.GameType;

/**
 * 游戏大类Service接口
 * 
 * @author hxp
 * @date 2021-12-03
 */
public interface IGameTypeService 
{
    /**
     * 查询游戏大类
     * 
     * @param id 游戏大类主键
     * @return 游戏大类
     */
    public GameType selectGameTypeById(Long id);

    /**
     * 查询游戏大类列表
     * 
     * @param gameType 游戏大类
     * @return 游戏大类集合
     */
    public List<GameType> selectGameTypeList(GameType gameType);

    /**
     * 新增游戏大类
     * 
     * @param gameType 游戏大类
     * @return 结果
     */
    public int insertGameType(GameType gameType);

    /**
     * 修改游戏大类
     * 
     * @param gameType 游戏大类
     * @return 结果
     */
    public int updateGameType(GameType gameType);

    /**
     * 批量删除游戏大类
     * 
     * @param ids 需要删除的游戏大类主键集合
     * @return 结果
     */
    public int deleteGameTypeByIds(String ids);

    /**
     * 删除游戏大类信息
     * 
     * @param id 游戏大类主键
     * @return 结果
     */
    public int deleteGameTypeById(Long id);
}
