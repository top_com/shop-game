package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.GameTypeMapper;
import com.ruoyi.system.domain.GameType;
import com.ruoyi.system.service.IGameTypeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 游戏大类Service业务层处理
 * 
 * @author hxp
 * @date 2021-12-03
 */
@Service
public class GameTypeServiceImpl implements IGameTypeService 
{
    @Autowired
    private GameTypeMapper gameTypeMapper;

    /**
     * 查询游戏大类
     * 
     * @param id 游戏大类主键
     * @return 游戏大类
     */
    @Override
    public GameType selectGameTypeById(Long id)
    {
        return gameTypeMapper.selectGameTypeById(id);
    }

    /**
     * 查询游戏大类列表
     * 
     * @param gameType 游戏大类
     * @return 游戏大类
     */
    @Override
    public List<GameType> selectGameTypeList(GameType gameType)
    {
        return gameTypeMapper.selectGameTypeList(gameType);
    }

    /**
     * 新增游戏大类
     * 
     * @param gameType 游戏大类
     * @return 结果
     */
    @Override
    public int insertGameType(GameType gameType)
    {
        gameType.setCreateTime(DateUtils.getNowDate());
        gameType.setCreateBy(ShiroUtils.getLoginName());
        return gameTypeMapper.insertGameType(gameType);
    }

    /**
     * 修改游戏大类
     * 
     * @param gameType 游戏大类
     * @return 结果
     */
    @Override
    public int updateGameType(GameType gameType)
    {
        gameType.setUpdateTime(DateUtils.getNowDate());
        gameType.setUpdateBy(ShiroUtils.getLoginName());
        return gameTypeMapper.updateGameType(gameType);
    }

    /**
     * 批量删除游戏大类
     * 
     * @param ids 需要删除的游戏大类主键
     * @return 结果
     */
    @Override
    public int deleteGameTypeByIds(String ids)
    {
        return gameTypeMapper.deleteGameTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除游戏大类信息
     * 
     * @param id 游戏大类主键
     * @return 结果
     */
    @Override
    public int deleteGameTypeById(Long id)
    {
        return gameTypeMapper.deleteGameTypeById(id);
    }
}
