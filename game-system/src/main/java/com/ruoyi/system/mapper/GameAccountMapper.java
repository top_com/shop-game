package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.GameAccount;

/**
 * 商品 - 出售账号的Mapper接口
 * 
 * @author hxp
 * @date 2021-12-07
 */
public interface GameAccountMapper 
{
    /**
     * 查询商品 - 出售账号的
     * 
     * @param id 商品 - 出售账号的主键
     * @return 商品 - 出售账号的
     */
    public GameAccount selectGameAccountById(Long id);

    /**
     * 查询商品 - 出售账号的列表
     * 
     * @param gameAccount 商品 - 出售账号的
     * @return 商品 - 出售账号的集合
     */
    public List<GameAccount> selectGameAccountList(GameAccount gameAccount);

    /**
     * 新增商品 - 出售账号的
     * 
     * @param gameAccount 商品 - 出售账号的
     * @return 结果
     */
    public int insertGameAccount(GameAccount gameAccount);

    /**
     * 修改商品 - 出售账号的
     * 
     * @param gameAccount 商品 - 出售账号的
     * @return 结果
     */
    public int updateGameAccount(GameAccount gameAccount);

    /**
     * 删除商品 - 出售账号的
     * 
     * @param id 商品 - 出售账号的主键
     * @return 结果
     */
    public int deleteGameAccountById(Long id);

    /**
     * 批量删除商品 - 出售账号的
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGameAccountByIds(String[] ids);
}
