package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.GameSpu;

/**
 * 游戏spuMapper接口
 * 
 * @author hxp
 * @date 2021-12-06
 */
public interface GameSpuMapper 
{
    /**
     * 查询游戏spu
     * 
     * @param id 游戏spu主键
     * @return 游戏spu
     */
    public GameSpu selectGameSpuById(Long id);

    /**
     * 查询游戏spu列表
     * 
     * @param gameSpu 游戏spu
     * @return 游戏spu集合
     */
    public List<GameSpu> selectGameSpuList(GameSpu gameSpu);

    /**
     * 新增游戏spu
     * 
     * @param gameSpu 游戏spu
     * @return 结果
     */
    public int insertGameSpu(GameSpu gameSpu);

    /**
     * 修改游戏spu
     * 
     * @param gameSpu 游戏spu
     * @return 结果
     */
    public int updateGameSpu(GameSpu gameSpu);

    /**
     * 删除游戏spu
     * 
     * @param id 游戏spu主键
     * @return 结果
     */
    public int deleteGameSpuById(Long id);

    /**
     * 批量删除游戏spu
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGameSpuByIds(String[] ids);
}
